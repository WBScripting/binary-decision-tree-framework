import scripts.wastedbro.api.decision_tree.DecisionTree;
import scripts.wastedbro.api.decision_tree.DecisionTreeBuilder;

/**
 * The lambda functionality of the tree builder allows you to create quick and easy nodes on the fly, without any methods or classes
 * Perfect for small bits of functionality
 */
public class LambdaExample
{
    public static void main(String[] args)
    {
        DecisionTree tree = new DecisionTreeBuilder()
            .onValidDecision(()-> true)
                .onValidAction(()-> System.out.println("Yay"))
                .onInvalidAction(()-> System.out.println("Boooo"))
            .end()
            // We don't need an onInvalid because the invisible top-level node always returns true
            .build();

        tree.getValidRunnable().run();
    }
}
