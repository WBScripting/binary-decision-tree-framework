import scripts.wastedbro.api.decision_tree.DecisionTree;
import scripts.wastedbro.api.decision_tree.DecisionTreeBuilder;

import java.util.function.Consumer;

/**
 * Tree splicing is how you can build a very large and complex trees using smaller, more maintainable trees
 * This is a good way to split up your script into modules
 */
public class SpliceExample
{
    public static void main(String[] args)
    {
        DecisionTree mainTree = new DecisionTreeBuilder()
            .onValidDecision(()-> false) // Notice that this decision node always reports FALSE, meaning it's invalid node will execute (module2)
                .onValidSplice(new Module1())
                .onInvalidSplice(new Module2())
            .end()
            // We don't need an onInvalid because the invisible top-level node always returns true
            .build();


        // while (true) {
        mainTree.getValidRunnable().run();
        // }
    }

    static class Module1 implements Consumer<DecisionTreeBuilder>
    {
        @Override
        public void accept(DecisionTreeBuilder decisionTreeBuilder)
        {
            decisionTreeBuilder
                .onValidDecision(()-> true)
                    .onValidAction(()-> System.out.println("Module 1 ran successfully"))
                    .onInvalidAction(()-> System.out.println("This won't print out"))
                .end();
        }
    }

    static class Module2 implements Consumer<DecisionTreeBuilder>
    {
        @Override
        public void accept(DecisionTreeBuilder decisionTreeBuilder)
        {
            decisionTreeBuilder
                .onValidDecision(()-> 5 == 5)
                    .onValidDecision(()-> true)
                        .onValidAction(()-> System.out.println("Module 2 ran successfully"))
                    .end()
                    .onInvalidAction(()-> System.out.println("This won't print out"))
                .end();
        }
    }
}
