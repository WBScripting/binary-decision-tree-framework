import scripts.wastedbro.api.decision_tree.DecisionTree;
import scripts.wastedbro.api.decision_tree.DecisionTreeBuilder;

/**
 * It's easy to incorporate methods into the decision tree, via lambda
 * This is useful if you want to add functionality to the tree that isn't quite large enough for it's own class, but still too large for a lambda method (anonymous class)
 */
public class MethodExample
{
    public static void main(String[] args)
    {
        DecisionTree mainTree = new DecisionTreeBuilder()
            .onValidDecision(()-> isAtBank())
                .onValidAction(()-> doBank())
                .onInvalidAction(()-> walkToBank())
            .end()
            // We don't need an onInvalid because the invisible top-level node always returns true
            .build();


        // while (true) {
            mainTree.getValidRunnable().run();
        // }
    }

    private static boolean isAtBank()
    {
        // determine if we're at the bank
        return true;
    }

    private static void doBank()
    {
        System.out.println("Doing bank");
        // do bank stuff here
    }

    private static void walkToBank()
    {
        System.out.println("Walking to bank");
        // DaxWalker.walkTo(bankTile);
    }
}
