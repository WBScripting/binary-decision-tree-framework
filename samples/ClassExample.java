import scripts.wastedbro.api.decision_tree.DecisionTree;
import scripts.wastedbro.api.decision_tree.DecisionTreeBuilder;

import java.util.function.BooleanSupplier;

/**
 * The DecisionTreeBuilder allows you to build a decision tree using nodes that you create.
 * You can represent your nodes via classes if you inherit from the correct types. This examples shows you how.
 */
public class ClassExample
{
    public static void main(String[] args)
    {
        DecisionTree mainTree = new DecisionTreeBuilder()
            .onValidDecision(new IsAtBankDecisionNode())
                .onValidAction(new BankAction())
                .onInvalidAction(new WalkToBankAction())
            .end()
            // We don't need an onInvalid because the invisible top-level node always returns true
            .build();


        // while (true) {
        mainTree.getValidRunnable().run();
        // }
    }

    // Decision nodes are BooleanSuppliers as far as the builder is concerned
    static class IsAtBankDecisionNode implements BooleanSupplier
    {
        @Override
        public boolean getAsBoolean()
        {
            // determine if we're at the bank
            return false;
        }
    }

    // Actions are simply Runnables
    static class BankAction implements Runnable
    {
        @Override
        public void run()
        {
            System.out.println("Doing bank");
            // do bank stuff here
        }
    }

    static class WalkToBankAction implements Runnable
    {
        @Override
        public void run()
        {
            System.out.println("Walking to bank");
            // DaxWalker.walkTo(bankTile);
        }
    }
}
